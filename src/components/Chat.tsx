'use client'

import { Avatar, AvatarFallback, AvatarImage } from "./ui/avatar";
import { Button } from "./ui/button";
import { Card, CardContent, CardDescription, CardFooter, CardHeader, CardTitle } from "./ui/card";
import { Input } from "./ui/input";
import { useChat } from 'ai/react';
import { ScrollArea } from "./ui/scroll-area";

export function Chat() {

    const { messages, input, handleSubmit, handleInputChange } = useChat({
        api: '/api/chat'
    })

    return (
        <Card className='w-[440px] h-[700px] grid grid-rows-[min-content_1fr_min-content]'>
            <CardHeader>
                <CardTitle>Chat AI</CardTitle>
                <CardDescription>Using Vercel SDK to create a chat bot.</CardDescription>
            </CardHeader>
            <CardContent className='space-y-4'>
                <ScrollArea>
                    {messages.map(message => (
                        <div key={message.id} className='flex gap-3 text-slate-600 text-sm'>
                            {message.role === 'user' && (
                                <Avatar>
                                    <AvatarFallback>DF</AvatarFallback>
                                    <AvatarImage src='https://images.jdmagicbox.com/comp/anand/t5/9999p2692.2692.170610113006.h5t5/catalogue/avatar-generic-medicines-store-vallabh-vidyanagar-anand-chemists-vpz86x.jpg' />
                                </Avatar>
                            )}

                            {message.role === 'assistant' && (
                                <Avatar>
                                    <AvatarFallback>Skychad</AvatarFallback>
                                    <AvatarImage src='https://media.licdn.com/dms/image/D5612AQH79uIW8BtQfA/article-cover_image-shrink_720_1280/0/1679714298344?e=2147483647&v=beta&t=6mcbxsOtr3bWqx35Uvsm6__KxPq9soMxf70M62_0RgU' />
                                </Avatar>
                            )}
                            
                            <p className='leading-relaxed'>
                                <span className='block font-bold text-slate-700'>
                                    {message.role === 'user' ? 'Uman' : 'AI'}
                                </span>
                                {message.content}
                            </p>
                        </div>
                    ))}
                </ScrollArea>
            </CardContent>
            <CardFooter>
                <form className="w-full flex gap-2" onSubmit={handleSubmit}>
                    <Input placeholder="How can I help you?" value={input} onChange={handleInputChange} />
                    <Button type="submit">Send</Button>
                </form>
            </CardFooter>
        </Card>
    )
}